package pw.bowser.testboot

import pw.bowser.util.VersionData

/**
 * Date: 2/21/14
 * Time: 10:43 PM
 */
object TestingVersionData extends VersionData {

  /**
   * Get the version string for the software
   *
   * @return version
   */
  def version: String = "Testing"

  /**
   * Get the VCS revision id for the software, if applicable.
   *
   * @return revision
   */
  def vcsRevision: String = "HEAD"

  /**
   * Get the VCS revision time for the software, if applicable
   *
   * @return revision date
   */
  def vcsRevisionTime: String = "Now"

  /**
   * Get the time at which the software was built
   *
   * @return build time
   */
  def buildTime: String = "Now"

}
