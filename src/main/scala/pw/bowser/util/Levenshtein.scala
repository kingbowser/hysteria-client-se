package pw.bowser.util

import java.util

/**
 * Adds access to levenshtein dist
 *
 * Date: 2/16/14
 * Time: 8:00 PM
 */
object Levenshtein {

  def apply(fromStr: String, toStr: String): Int = {
    def min(num: Int*) = num.min

    val distMatrix: Array[Array[Int]] =  Array.ofDim(fromStr.length + 1, toString.length + 1)

    (0 to fromStr.length).foreach({ charPos =>
      distMatrix(charPos)(0) = charPos
    })
    (0 to toStr.length).foreach({ charPos =>
      distMatrix(0)(charPos) = charPos
    })

    for(col <- 1 to fromStr.length; row <- 1 to toStr.length) {
      distMatrix(col)(row) = min (
        distMatrix(col -1)(row    ) + 1,
        distMatrix(col   )(row - 1) + 1,
        distMatrix(col -1)(row - 1) + (if(fromStr(col - 1) == toStr(row - 1)) 0 else 1)
      )
    }

    distMatrix(fromStr.length)(toStr.length)
  }

}