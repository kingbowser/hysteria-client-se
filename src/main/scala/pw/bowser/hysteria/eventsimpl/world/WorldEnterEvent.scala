package pw.bowser.hysteria.eventsimpl.world

import net.minecraft.world.World
import pw.bowser.hysteria.events.PersistentEvent

/**
 * Event broadcast when a world/server is entered
 *
 * Date: 2/13/14
 * Time: 10:27 PM
 */
class WorldEnterEvent(world: World,
                      var hostname: String = "localhost",
                      var port: Int = 25565) extends PersistentEvent {

  def getName: String = "WorldEnter"

}
