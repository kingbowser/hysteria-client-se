package pw.bowser.hysteria.eventsimpl.world

import net.minecraft.world.World
import pw.bowser.hysteria.events.Event

/**
 * Event that is broadcast when anything that could cause a world exit (except for a crash) happens
 *
 * Date: 2/13/14
 * Time: 10:28 PM
 */
class WorldPreExitEvent(world: World,
                        hostName: String = "localhost",
                        port: Int = 25565) extends Event {

  def getName: String = "WorldPreExit"
}
