package pw.bowser.hysteria.eventsimpl.world

import net.minecraft.entity.Entity
import pw.bowser.hysteria.events.PersistentEvent

/**
 * Event published when an entity dies
 *
 * Date: 2/13/14
 * Time: 9:58 PM
 */
class EntityDeathEvent(killed: Entity, killer: Option[Entity]) extends PersistentEvent {
  def getName: String = "EntityDeath"
}
