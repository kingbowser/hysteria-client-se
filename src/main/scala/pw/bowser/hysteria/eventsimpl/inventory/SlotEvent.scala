package pw.bowser.hysteria.eventsimpl.inventory

import pw.bowser.hysteria.events.Event
import net.minecraft.inventory.Container

/**
 * Event about a slot
 * Date: 3/14/14
 * Time: 8:56 PM
 */
abstract class SlotEvent(var container: Container, var slotId:    Int) extends Event
