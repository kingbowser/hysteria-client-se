package pw.bowser.hysteria.eventsimpl.minecraft.player

import pw.bowser.hysteria.events.Event

/**
 * Sent before a motionupate
 *
 * Date: 5/16/14
 * Time: 11:01 PM
 */
class PreMotionUpdate extends Event {
  override def getName: String = "PreMotionUpdate"
}
