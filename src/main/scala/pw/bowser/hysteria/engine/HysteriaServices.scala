package pw.bowser.hysteria.engine

import pw.bowser.hysteria.util.saving.GlobalStorage
import pw.bowser.minecraft.MinecraftToolkit
import pw.bowser.hysteria.minecraft.{RenderQueue, TickGroup}
import pw.bowser.minecraft.wrapper.net.minecraft.entity.player.HEntityPlayer
import pw.hysteria.input.dashfoo.command.CommandDispatcher
import pw.bowser.hysteria.gui.overlay.TextHUD
import pw.bowser.hysteria.events.EventHub
import net.minecraft.client.Minecraft
import pw.bowser.minecraft.players.{PlayerData, PlayerDataManager}
import net.minecraft.entity.player.EntityPlayer
import pw.bowser.hysteria.keyboard.BindingManager
import pw.bowser.minecraft.servers.{ServerDataManager, ServerData}
import pw.bowser.libhysteria.config.Configuration
import pw.bowser.libhysteria.toggles.ToggleService

/**
 * Hysteria services mixin (tellplayer, etc)
 * Date: 2/6/14
 * Time: 11:46 AM
 */
trait HysteriaServices {

  import pw.bowser.minecraft.wrapper.WrapperConversions._

  /**
   * Tell the player something
   * 
   * @param text text
   */
  protected final def tellPlayer(text: String) = HysteriaBroker.hysteria.toolkit.tellPlayer(text)

  /**
   * Say something in chat as the player
   * 
   * @param text message
   */
  protected final def sayText(text: String) = HysteriaBroker.hysteria.toolkit.sendChatMessage(text)

  /**
   * Get the Hysteria Configuration (for the engine)
   * 
   * @return engine configuration
   */
  protected final def engineConfiguration: Configuration = HysteriaBroker.hysteria.configuration

  /**
   * Save GlobalStorage
   */
  protected def saveAll() = GlobalStorage.save()

  /**
   * Load GlobalStorage
   */
  protected def loadAll() = GlobalStorage.load()

  /**
   * Get the MinecraftToolkit implementation
   * 
   * @return toolkit
   */
  protected final def minecraftToolkit: MinecraftToolkit = HysteriaBroker.hysteria.toolkit

  /**
   * Get the instance of minecraft for this engine instance
   * 
   * @return minecraft
   */
  protected final def minecraft: Minecraft = HysteriaBroker.hysteria.minecraft

  /*
   * Services
   */

  /**
   * Get the client-wide toggle registry that manages Toggles and persistence
   * 
   * @return toggle registry
   */
  protected final implicit def toggleRegistry: ToggleService = HysteriaBroker.hysteria.toggleService

  /**
   * Get the tick service for mods that need to do something every tick
   *
   * @return tick service
   */
  protected final def tickService: TickGroup = HysteriaBroker.hysteria.tickGroup

  /**
   * Get the RenderQueue for mods that need to add RenderJobs to the in-game view
   *
   * @return Render Queue
   */
  protected final def renderQueue: RenderQueue = HysteriaBroker.hysteria.renderQueue

  /**
   * Get the RenderQueue for mods that need to add RenderJobs to the lobby (out-of-game) view
   *
   * @return lobby render queue
   */
  protected final def lobbyRenderQueue: RenderQueue = HysteriaBroker.hysteria.renderQueueOutOfGame

  /**
   * Get the RenderQueue for mods that need to add RenderJobs to the 3D world view
   *
   * @return lobby render queue
   */
  protected final def worldRenderQueue: RenderQueue = HysteriaBroker.hysteria.renderQueuePostWorld

  /**
   * Get the DashFoo command dispatcher instance
   *
   * @return command dispatcher
   */
  protected final def commandDispatcher: CommandDispatcher = HysteriaBroker.hysteria.commandDispatcher

  /**
   * Get the in-game heads-up-display for text, this allows mods to attach blocks of text to one of four corners on-screen
   *
   * @return heads-up display
   */
  protected final def headsUpDisplay: TextHUD = HysteriaBroker.hysteria.overlay

  /**
   * Get the key binding management service
   *
   * @return keyboard service provider
   */
  protected final def keyboard: BindingManager = HysteriaBroker.hysteria.keybinds

  /**
   * Get the event system used to broadcast events to subscribers
   *
   * @return event system
   */
  protected def eventSystem: EventHub = HysteriaBroker.hysteria.events

  /**
   * Get the player data service for looking up, adding data to, and managing players
   *
   * @return player data manager
   */
  protected final def playerData: PlayerDataManager = HysteriaBroker.hysteria.playerData

  /**
   * Get the server data manager
   *
   * @return server data manager
   */
  protected final def serverDataManager: ServerDataManager = HysteriaBroker.hysteria.serverData

  /**
   * Get the current server
   *
   * @return current server
   */
  protected final def currentServer: Option[ServerData] = HysteriaBroker.hysteria.currentServer

  /**
   * Allows the simple lookup of player data
   */
  object PlayerDataFor {

    /**
     * Get player data for a player entity
     *
     * @param player  player entity
     * @return        data
     * @deprecated Please use [[HEntityPlayer HEntityPlayer]]
     */
    def apply(player: EntityPlayer): PlayerData = player.playerData

    /**
     * Get player data for a username
     *
     * @param name  player name
     * @return      data
     */
    def apply(name: String): PlayerData = playerData.lookupPlayer(name)
    
  }

}
