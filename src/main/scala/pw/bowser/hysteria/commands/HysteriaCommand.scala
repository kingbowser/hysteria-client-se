package pw.bowser.hysteria.commands

import pw.hysteria.input.dashfoo.command._
import pw.hysteria.input.dashfoo.command.parsing.{CommandRegexProvider, CommandFormattingProvider, UserInputFlagParser}
import pw.hysteria.input.dashfoo.command.parsing.impl.SimpleRegexProvider
import java.util

/**
 * A hybrid flagged command
 *
 * Date: 2/4/14
 * Time: 3:24 PM
 */
abstract class HysteriaCommand(handles: Array[String], regexProvider: CommandRegexProvider) extends Command with HasHelpProvider with HelpProvider {

  protected val DEFAULT_FLAG_NAME = "@default"
  protected val IF_NO_OTHERS_FLAG = "@if_none"

  /**
   * Provides flag parsing services
   */
  val parser: UserInputFlagParser = new NonStrictFlagParser(regexProvider)

  /**
   * Handles running of action flags
   */
  val flagger = new NonStrictFlagDispatcher

  final def invokeCommand(commandText: String) {
    val mappings = parser.mapUserInput(commandText)

    if(mappings.size() == 1 && flagger.getFlags.containsKey(IF_NO_OTHERS_FLAG)){ // @default is the only flag?
      // If so, instead of invoking @default, invoke @ifnone
      val argsDefault = mappings.get(DEFAULT_FLAG_NAME)
      mappings.put(IF_NO_OTHERS_FLAG, argsDefault)
      mappings.remove(DEFAULT_FLAG_NAME)
    }

    flagger.dispatch(mappings)
    onCommand(mappings.get(DEFAULT_FLAG_NAME), mappings)
  }

  /**
   * Deferred command invocation
   *
   * @param commandArgs default flag arguments
   * @param flags       flags and their arguments
   */
  def onCommand(commandArgs: Array[String], flags: util.Map[String, Array[String]]) = {}

  final def getHandles: Array[String]     = handles

  final def getHelpProvider: HelpProvider = this

  def initialize() {
    val methods = (new MethodAsInvokableFactory).constructFromClass(this)
    flagger.addAll(methods)
  }

}
