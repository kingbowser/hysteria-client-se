package pw.bowser.hysteria.minecraft.abstractionlayer

import net.minecraft.item.{Item, ItemStack}
import net.minecraft.client.Minecraft
import net.minecraft.network.play.client.{C0EPacketClickWindow, C0DPacketCloseWindow, C16PacketClientStatus}

/**
 * Simplifies interaction with player inventory
 * Date: 3/14/14
 * Time: 3:47 PM
 */
final class PlayerInventoryToolkit(minecraft: Minecraft) {

  private def inventory   = minecraft.thePlayer.inventory
  private def player      = minecraft.thePlayer
  private def controller  = minecraft.playerController

  /**
   * Get the contents of a slot in the manged inventory
   *
   * @param id  slot id
   * @return    slot contents
   */
  def getSlotContents(id: Int): Option[ItemStack] = {
    if(id < 0 || id > PlayerInventoryToolkit.INVENTORY_MAX_SLOT) throw new IllegalArgumentException(s"Invalid Slot ID ($id)")

    inventory.getStackInSlot(id) match {
      case i: ItemStack => Some(i)
      case null         => None
    }
  }

  /**
   * Find the first slot number (and associated ItemStack) containing the specified item id
   * @param itemId item id
   * @return (Stack, Slot Number)
   */
  def firstSlotWithId(itemId: Int): Option[(ItemStack, Int)] =
    inventory.mainInventory.zipWithIndex.find({
      case(null, _) => false
      case(stack, index) =>
        Item.getIdFromItem(stack.getItem) == itemId
    })

  /**
   * Find the fist slot and associated item stack containing the specified item
   * @param item item
   * @return (Stack, Slot Number)
   */
  def firstSlotWithItem(item: Item): Option[(ItemStack, Int)] = firstSlotWithId(Item.getIdFromItem(item))

  /**
   * Move an item from one slot to another
   * If the destination slot is full, the contents will be moved to the first empty slot
   * @param slotFrom slot to transfer contents from
   * @param slotTo   slot to transfer contents to
   * @return success
   */
  def moveItem(slotFrom: Int, slotTo: Int): Boolean = {
    if(slotFrom > PlayerInventoryToolkit.INVENTORY_MAX_SLOT || slotFrom < 0) throw new IllegalArgumentException(s"Invalid origin slot ($slotFrom)")
    if(slotTo   > PlayerInventoryToolkit.INVENTORY_MAX_SLOT || slotTo   < 0) throw new IllegalArgumentException(s"Invalid destination slot ($slotTo)")

    // If the destination is occupied move the contents of the destination to the next available slot
    if(getSlotContents(slotTo) != None) {
      val firstSlotTuple = firstSlotWithId(0)
      firstSlotTuple match {
        case t: Some[(ItemStack, Int)] => moveItem(slotTo, t.get._2)
        case None => return false
      }
    }

    minecraft.getNetHandler.addToSendQueue(new C16PacketClientStatus(C16PacketClientStatus.EnumState.OPEN_INVENTORY_ACHIEVEMENT))

    controller.windowClick(0, slotFrom, 0, 0, player)
    controller.windowClick(0, slotTo  , 0, 0, player)
    controller.windowClick(0, slotFrom, 0, 0, player)
    minecraft.getNetHandler.addToSendQueue(new C0DPacketCloseWindow(0))
    true
  }

  /**
   * Drop the contents of a slot
   * @param slot slot
   */
  def dropSlotContents(slot: Int) {
    minecraft.getNetHandler.addToSendQueue(new C16PacketClientStatus(C16PacketClientStatus.EnumState.OPEN_INVENTORY_ACHIEVEMENT))
    controller.windowClick(0, slot, 0, 0, player)
    controller.windowClick(0, -999, 0, 0, player)
    minecraft.getNetHandler.addToSendQueue(new C0DPacketCloseWindow(0))
  }

  /**
   * Click in the player inventory
   * @param slotId    slot number
   * @param buttonId  mouse button
   * @param action    action
   */
  def inventoryClick(slotId: Int, buttonId: Int, action: Int) {
    val transId = player.inventoryContainer.getNextTransactionID(player.inventory)
    val stack   = player.inventory.getStackInSlot(slotId)
    minecraft.getNetHandler.addToSendQueue(new C0EPacketClickWindow(0, slotId, buttonId, action, stack, transId))
  }

  /**
   * Check if the player has the specified item in the inventory
   * @param item item
   * @return has item
   */
  def hasItem(item: Item): Boolean = player.inventory.hasItem(item)

  /**
   * Check if the player has the specified itemStack in the inventory
   * @param stack stack
   * @return has stack
   */
  def hasItemStack(stack: ItemStack): Boolean = player.inventory.hasItemStack(stack)

  /**
   * True if the player is using an item
   * @return using item
   */
  def isUsingItem: Boolean = player.isUsingItem

  /**
   * Stop using the item in hand
   */
  def stopUsingItem(): Unit = player.stopUsingItem()

  /**
   * Current itemStack
   * @return itemStack
   */
  def currentItemStack: ItemStack = player.inventory.getCurrentItem

  /**
   * Current slot
   * @return current slot
   */
  def currentHotBarSlot: Int = player.inventory.currentItem
}

object PlayerInventoryToolkit {

  val INVENTORY_MAX_SLOT  = 36
  val HOT_BAR_MAX_SLOT    = 8

}
