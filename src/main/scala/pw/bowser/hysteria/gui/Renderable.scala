package pw.bowser.hysteria.gui

import pw.bowser.hysteria.minecraft.RenderJob

/**
 * RenderJob companion
 *
 * Date: 2/3/14
 * Time: 7:09 PM
 */
object Renderable {

  def apply(job: => Unit): RenderJob = new RenderJob{
    def render(): Unit = job
  }

}
