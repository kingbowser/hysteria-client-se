package pw.bowser.hysteria.module.internal.util

import pw.bowser.hysteria.minecraft.TickReceiver
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.toggles.TogglesMixin
import net.minecraft.client.settings.KeyBinding

/**
 * Date: 3/23/14
 * Time: 5:40 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "AutoMine",
  version     = "0.1",
  commands    = Array("am"),
  description = "Allows you to stand in front of a block and break it. Good for Citadel."
)
class AutoMine extends HysteriaModule with TickReceiver with TogglesMixin {



  val command = Command(Array("am"), ToggleFlag(Array("@if_none", "t"), this))


  override def enableModule(): Unit = {
    super.enableModule()

    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)
  }

  def onTick(): Unit = if(minecraft.theWorld != null && isEnabled) {
    KeyBinding.setKeyBindState(minecraft.gameSettings.keyBindAttack.getKeyCode, true)
  }

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "AutoMine"

  override def enable(): Unit = {
    tickService.register(this)
  }

  override def disable(): Unit = {
    tickService.remove(this)
    KeyBinding.setKeyBindState(minecraft.gameSettings.keyBindAttack.getKeyCode, false)
  }

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.auto_mine"
}
