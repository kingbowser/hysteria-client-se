package pw.bowser.hysteria.module.internal.hacks.other

import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * AntiCactus
 * @author zencantsnipe
 * Date: 4/27/14
 * Time: 11:57 AM
 */
@ModuleManifest(
  groupId     = "pw.hysteria.zencantsnipe",
  name        = "AntiCactus",
  version     = "0.1",
  commands    = Array("anticactus", "ac"),
  description = "Sets the bounding box of cactus to the same as a solid block"
)
class AntiCactus extends HysteriaModule with TogglesMixin {

  private val commandAntiCactus =
    Command(Array("ac", "anticactus"),
      ToggleFlag(Array("@if_none", "@default"), this)
    )

  override def enableModule(): Unit = {
    super.enableModule()
    commandDispatcher.registerCommand(commandAntiCactus)
    toggleRegistry.enrollToggleable(this)
  }

  override def disableModule(): Unit = {
    super.disableModule()
    commandDispatcher.unRegisterCommand(commandAntiCactus)
    toggleRegistry.disenrollToggleable(this)
  }

  def displayName: String = "AntiCactus"

  def distinguishedName: String = "pw.hysteria.zencantsnipe.anti_cactus"

}
