package pw.bowser.hysteria.module.internal.hacks.world.fly

import pw.bowser.hysteria.commands.companion.{Flag, Command}
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.chat.Formatting
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * Date: 2/27/14
 * Time: 9:48 PM
 */
@ModuleManifest(
  groupId = "pw.bowser",
  name = "Fly",
  version = "0.1",
  commands = Array("fly"),
  description = "Planar and Parabolic fly"
)
class ModFly extends HysteriaModule with TogglesMixin {

  private var command: HysteriaCommand = null

  override def initModule(): Unit = {
    super.initModule()

    configuration.setPropertyIfNew("fly.mode",   "3d")
    configuration.setPropertyIfNew("fly.wedge",  0.2D)

    command = Command(
      Array("fly"),
      ToggleFlag(Array("@if_none"), this),
      Flag(Array("mode", "m"), {
        args =>
          if (args.length > 0) configuration.setProperty("fly.mode", args(0) match {
            case "3d" | "look" => "3d"
            case "planar" | "notch" | "flat" => "planar"
            case _ =>
              tellPlayer("Invalid Fly Mode")
              configuration.getString("fly.mode")
          })

          tellPlayer(s"Fly Mode is set to ${Formatting.italicize(configuration.getString("fly.mode"))}")
      }),
      ConfigurationFlag(Array("w", "wedge"), configuration, "fly.wedge", ConfigurationFlag.Transforms.DOUBLE)
    )
  }

  override def enableModule(): Unit = {

    toggleRegistry.enrollToggleable(this)
    commandDispatcher.registerCommand(command)

    super.enableModule()
  }

  override def disableModule(): Unit = {

    toggleRegistry.disenrollToggleable(this)
    commandDispatcher.unRegisterCommand(command)

    super.disableModule()
  }

  def flyImpl: FlyMode = configuration.getString("fly.mode") match {
    case "3d" => Fly3D
    case "planar" | _ => PlanarFly
  }

  def flyWedge: Double = configuration.getDouble("fly.wedge")

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "Fly"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.fly"

  override def conflictingToggles: List[String] = List("pw.hysteria.sprint", "pw.hysteria.speed", "pw.hysteria.shusako.glide")

  override def statusText: Option[String] = Some(configuration.getString("fly.mode") + " " + configuration.getDouble("fly.wedge"))
}
