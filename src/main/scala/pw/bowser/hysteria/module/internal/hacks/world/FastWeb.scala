package pw.bowser.hysteria.module.internal.hacks.world

import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.toggles.{ToggleState, TogglesMixin}
import pw.bowser.hysteria.module.internal.HysteriaModule

/**
 * Date: 2/20/14
 * Time: 10:53 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "FastWeb",
  version     = "0.1",
  commands    = Array("fw", "nw"),
  description = "Eases navigation in webs"
)
class FastWeb extends HysteriaModule with TogglesMixin {

  override def initModule(): Unit = {
    configuration.setPropertyIfNew("fastweb.nc_friendly",  true)

    commandDispatcher.registerCommand(
      Command(
        Array("fw", "nw"),
        ToggleFlag(Array("@if_none"), this),
        ConfigurationFlag(Array("u", "nc", "set_unsafe"), configuration, "fastweb.nc_friendly", {(args: Array[String]) => args(0).toLowerCase == "true"})
      )
    )

    toggleRegistry.enrollToggleable(this)
  }

  def getMovementCoeficient: Double = getState == ToggleState.Enabled match {
    case true   =>
//      0.967471743D
      0.97
//      1.0D
    case false  => 0.25D
  }

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "FastWeb"

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.fast_web"
}
