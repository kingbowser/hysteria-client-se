package pw.bowser.hysteria.module.internal.util

import pw.bowser.hysteria.events.EventClient
import pw.bowser.hysteria.eventsimpl.inventory.SlotUpdateEvent
import pw.bowser.hysteria.commands.companion.{Flag, Command}
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.minecraft.MinecraftToolkit
import net.minecraft.item.Item
import scala.collection.{mutable => m}
import java.io.{OutputStream, InputStream}
import scala.util.parsing.json.{JSON, JSONArray}
import pw.bowser.hysteria.util.scala.json.Formatters
import scala.io.Source
import net.minecraft.client.resources.I18n
import pw.bowser.hysteria.eventsimpl.network.PacketReceiveEvent
import net.minecraft.network.play.server.S0DPacketCollectItem
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.libhysteria.toggles.TogglesMixin
import pw.bowser.libhysteria.storage.marshal.Marshalling

/**
 * Throws items out of the inventory when they are picked up.
 * Please note that this will not throw items of the specified type(s) already in the inventory.
 * Date: 3/15/14
 * Time: 9:06 PM
 */
@ModuleManifest(
  groupId     = "pw.bowser",
  name        = "RejectItems",
  version     = "0.1",
  commands    = Array("reji", "ii"),
  description = "Drop items that you don't want."
)
class RejectItems extends HysteriaModule with TogglesMixin with Marshalling {



  private var itemsIgnoring       = m.MutableList[Int]()

  private val command = Command(Array("reji", "ii"),
    ToggleFlag(Array("@if_none", "t"), this),
    Flag(Array("clear"), {
      args => itemsIgnoring.clear(); tellPlayer("Cleared Item List")
    }, null, "Clear the list of rejectable items"),
    Flag(Array("list", "l"), {
      args =>
        tellPlayer("Rejecting Items: " + itemsIgnoring.map(int =>
          if(Item.getItemById(int).getUnlocalizedName != null) I18n.format(Item.getItemById(int).getUnlocalizedName + ".name") else int.toString
        ).mkString(", "))
    }, null, "List the rejectable items"),
    Flag(Array("add", "a"), {
      args =>
        var countAdded = 0
        args.foreach({
          str =>
            MinecraftToolkit.StringTransforms.Item(str) match {
              case i: Some[Item] if !itemsIgnoring.contains(Item.getIdFromItem(i.get)) =>
                itemsIgnoring += Item.getIdFromItem(i.get)
                countAdded += 1
              case _ =>
            }
        })

        tellPlayer(s"Added $countAdded items")
    }, "<Item...>", "Add items to the list of things to reject"),
    Flag(Array("delete", "d"), {
      args =>
        var countRemoved = 0
        args.foreach({
          str =>
            MinecraftToolkit.StringTransforms.Item(str) match {
              case i: Some[Item] if itemsIgnoring.contains(Item.getIdFromItem(i.get)) =>
                itemsIgnoring = itemsIgnoring.filter(int => int != Item.getIdFromItem(i.get))
                countRemoved += 1
              case _ =>
            }
        })

        tellPlayer(s"Removed $countRemoved items")
    }, "<Item...>", "Remove items from the list of things to reject")
  )

  private val eventInvChange = EventClient[SlotUpdateEvent]({event =>
    if(event.container == minecraft.thePlayer.inventoryContainer
       && event.newStack.isDefined && itemsIgnoring.contains(Item.getIdFromItem(event.newStack.get.getItem))
       && isEnabled) {
      minecraftToolkit.inventoryToolkit.dropSlotContents(event.slotId)
//      event.cancel()
    }
  })

  private val eventCollect = EventClient[PacketReceiveEvent]{event =>
    event.packet match {
      case collect: S0DPacketCollectItem if isEnabled => event.cancel()
      case _ =>
    }
  }

  // Module Implementation
  //--------------------------------------------------------------------------------------------------------------------

  override def initModule(): Unit = {
    super.initModule()
  }

  override def enableModule(): Unit = {
    getStorageService.enroll(this)
    super.enableModule()

    commandDispatcher.registerCommand(command)
    toggleRegistry.enrollToggleable(this)
    eventSystem.subscribe(classOf[SlotUpdateEvent], eventInvChange)
    eventSystem.subscribe(classOf[PacketReceiveEvent], eventCollect)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    commandDispatcher.unRegisterCommand(command)
    toggleRegistry.disenrollToggleable(this)
    eventSystem.unSubscribe(classOf[SlotUpdateEvent], eventInvChange)
    eventSystem.unSubscribe(classOf[PacketReceiveEvent], eventCollect)
    getStorageService.disenroll(this)
  }

  // Helper Methods
  //--------------------------------------------------------------------------------------------------------------------



  // Toggles Implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "RejectItems"

  def distinguishedName: String = "pw.hysteria.reject_items"

  // Marshalling Implementation
  //--------------------------------------------------------------------------------------------------------------------

  /**
   * Marshal self to output stream
   *
   * @param stream stream to write to
   */
  def writeTo(stream: OutputStream): Unit = stream.write(JSONArray(itemsIgnoring.toList).toString(Formatters.prettyFormatter).getBytes)

  /**
   * Load self from input stream
   *
   * @param stream stream to read from
   */
  def readFrom(stream: InputStream): Unit = {
    val sourceStr = Source.createBufferedSource(stream).getLines().mkString("\n")
    val jsonPrim  = JSON.parseRaw(sourceStr)
    jsonPrim match {
      case sl: Some[JSONArray] =>
        itemsIgnoring.clear()
        sl.get.list.foreach({i =>
          itemsIgnoring += i.asInstanceOf[Double].toInt
        })
      case _ =>
    }
  }

  /**
   * Get the handle of the marshallable (for ref like filename etc)
   *
   * @return handle
   */
  def getHandle: String = "ignored_items"
}