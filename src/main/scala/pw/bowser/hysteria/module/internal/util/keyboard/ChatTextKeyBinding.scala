package pw.bowser.hysteria.module.internal.util.keyboard

import pw.bowser.hysteria.keyboard.KeyBinding
import pw.bowser.minecraft.MinecraftToolkit
import com.google.gson._
import java.lang.reflect.Type
import org.lwjgl.input.Keyboard

/**
 * A key binding that sends a chat message to the server
 * Date: 2/19/14
 * Time: 9:33 AM
 */
class ChatTextKeyBinding(toolkit: MinecraftToolkit,
                         val text: String,
                         val sequence: Array[Int]) extends KeyBinding(sequence) {
  /**
   * Get a description of the key binding
   *
   * @return description
   */
  def description: String = s"Says `$text` as the player"

  /**
   * Invoke the key binding action
   */
  def onDepress(): Unit = toolkit.sendChatMessage(text)

  final override def isToggle: Boolean = false

  override def toString: String = s"ChatTextKeyBinding(Array(${sequence.map(Keyboard.getKeyName).mkString(", ")}), $text)"

}


