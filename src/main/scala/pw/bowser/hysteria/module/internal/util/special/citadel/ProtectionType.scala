package pw.bowser.hysteria.module.internal.util.special.citadel

/**
 * Defines the sort of citadel protection applied to a block
 * Date: 3/24/14
 * Time: 11:12 PM
 */
sealed abstract case class ProtectionType(name: String, resilience: Int)
object ProtectionType{

  object UNKNOWN     extends ProtectionType("unknown",     -1  )
  object STONE       extends ProtectionType("stone",       25  )
  object IRON_INGOT  extends ProtectionType("iron ingot",  250 )
  object DIAMOND     extends ProtectionType("diamond",     1800)

  def withName(name: String): ProtectionType = name match {
    case "stone"      => STONE
    case "iron ingot" => IRON_INGOT
    case "diamond"    => DIAMOND
    case "unknown"    => UNKNOWN
    case _ => throw new NoSuchElementException(s"No such ProtectionType as $name exists")
  }

  sealed abstract case class ProtectionLevel(levelName: String)
  object ProtectionLevel {

    object UNKNOWN      extends ProtectionLevel("unknown")
    object EXCELLENTLY  extends ProtectionLevel("excellently")
    object WELL         extends ProtectionLevel("well")
    object DECENTLY     extends ProtectionLevel("decently")
    object POORLY       extends ProtectionLevel("poorly")

    def withName(name: String): ProtectionLevel = name match {
      case "excellently"  => EXCELLENTLY
      case "well"         => WELL
      case "decently"     => DECENTLY
      case "poorly"       => POORLY
      case "unknown"      => UNKNOWN
      case _ => throw new NoSuchElementException(s"No such ProtectionLevel as $name exists")
    }

  }

}
