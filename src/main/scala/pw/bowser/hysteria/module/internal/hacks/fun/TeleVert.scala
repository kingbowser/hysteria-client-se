package pw.bowser.hysteria.module.internal.hacks.fun

import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.commands.companion.{Flag, Command}
import pw.bowser.libhysteria.modules.ModuleManifest

/**
 * Date: 3/13/14
 * Time: 10:52 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "TeleVert",
  version     = "0.1",
  commands    = Array("up", "down"),
  description = "Teleport up/down. Probably patched"
)
class TeleVert extends HysteriaModule {

  private val commandUp = Command(Array("up"), "<Int>", "Teleport Up",
    Flag(Array("@default"), {args =>

      for(_c <- 0 to args(0).toInt - 1){
        minecraft.thePlayer.setPositionAndUpdate(minecraft.thePlayer.posX, minecraft.thePlayer.posY, minecraft.thePlayer.posZ)
      }

    }))

  private val commandDown = Command(Array("down"), "<Int>", "Teleport Down",
    Flag(Array("@default"), {args =>
      val goingDown = args(0).toDouble
      val hops      = goingDown / 7D
      val modulus   = goingDown % 7D

      minecraft.thePlayer.setPositionAndUpdate(minecraft.thePlayer.posX, minecraft.thePlayer.posY - args(0).toDouble, minecraft.thePlayer.posZ)}))

  override def enableModule(): Unit = {
    super.enableModule()

    commandDispatcher.registerCommand(commandUp)
    commandDispatcher.registerCommand(commandDown)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    commandDispatcher.unRegisterCommand(commandUp)
    commandDispatcher.unRegisterCommand(commandDown)
  }
}
