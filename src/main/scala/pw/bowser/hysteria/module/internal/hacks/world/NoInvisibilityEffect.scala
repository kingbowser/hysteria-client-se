package pw.bowser.hysteria.module.internal.hacks.world

import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.toggles.TogglesMixin

/**
 * Date: 2/20/14
 * Time: 2:22 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "NoInvisibility",
  version     = "0.1",
  commands    = Array("noie"),
  description = "Prevents the rendering engine from skipping over entities with the invisibility effect on"
)
class NoInvisibilityEffect extends HysteriaModule with TogglesMixin {

  override def initModule(): Unit = {
    super.initModule()

    commandDispatcher.registerCommand(Command(Array("noie"), null.asInstanceOf[String], "NoInvisibility", ToggleFlag(Array("@if_none"), this)))
    toggleRegistry.enrollToggleable(this)
  }

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  override def displayName: String = "NoInvisibility"

  /**
   * Shoudl return the distinguished name of the toggles
   * The disinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.no_invisibility"
}
