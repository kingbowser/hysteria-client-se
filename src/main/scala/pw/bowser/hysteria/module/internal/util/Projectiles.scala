package pw.bowser.hysteria.module.internal.util

// TODO via wrapper (!)
import net.minecraft.item.{ItemEgg, ItemEnderPearl, ItemSnowball, ItemBow}

import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.minecraft.RenderJob
import pw.bowser.minecraft.entities.ProjectilePathTrace

import java.lang.Math.{sin, cos, pow}
import pw.bowser.hysteria.display.GLUtil
import org.lwjgl.opengl.GL11
import org.lwjgl.util.glu.{Disk, GLU}
import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.commands.companion.Command
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.minecraft.entities.ProjectilePathTrace.ProjectileType
import pw.bowser.libhysteria.toggles.TogglesMixin
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.minecraft.wrapper.net.minecraft.render.entity.HRenderManager
import pw.bowser.minecraft.wrapper.net.minecraft.util.{HMovingObjectPosition, HEnumFacing}

/**
 * Date: 3/23/14
 * Time: 5:40 PM
 */
@ModuleManifest(
  groupId     = "pw.hysteria",
  name        = "Projectiles",
  version     = "0.1",
  commands    = Array("proj"),
  description = "Show projectile's path."
)
class Projectiles extends HysteriaModule with TogglesMixin with RenderJob {



  private var command: HysteriaCommand = null


  override def initModule(): Unit = {
    super.initModule()

    configuration.setPropertyIfNew("projectiles.render_arc",       false)
    configuration.setPropertyIfNew("projectiles.target_unlocked",  0xAAFFB849.toLong)
    configuration.setPropertyIfNew("projectiles.target_locked",    0xDD2FC729.toLong)

    command = Command(Array("proj"),
      ToggleFlag(Array("@if_none", "t"), this),
      ConfigurationFlag(Array("arc"),   configuration, "projectiles.render_arc",       ConfigurationFlag.Transforms.BOOLEAN),
      ConfigurationFlag(Array("cnorm"), configuration, "projectiles.target_unlocked",  ConfigurationFlag.Transforms.LONG),
      ConfigurationFlag(Array("chit"),  configuration, "projectiles.target_locked",    ConfigurationFlag.Transforms.LONG)
    )
  }

  override def enableModule(): Unit = {
    super.enableModule()

    commandDispatcher.registerCommand(command)
    toggleRegistry.enrollToggleable(this)
  }

  override def disableModule(): Unit = {
    super.disableModule()

    commandDispatcher.unRegisterCommand(command)
    toggleRegistry.disenrollToggleable(this)
  }

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String = "Projectiles"

  override def enable(): Unit = {
    worldRenderQueue.register(this)
  }

  override def disable(): Unit = {
    worldRenderQueue.remove(this)
  }

  // Render logic
  // ===================================================================================================================

  val disk     = new Disk
  disk.setDrawStyle(GLU.GLU_LINE)

  def render(): Unit = if(minecraft.gameSettings.thirdPersonView == 0 && minecraft.thePlayer.inventory.getCurrentItem != null
                          && (   minecraft.thePlayer.inventory.getCurrentItem.getItem.isInstanceOf[ItemBow]
                              || minecraft.thePlayer.inventory.getCurrentItem.getItem.isInstanceOf[ItemSnowball]
                              || minecraft.thePlayer.inventory.getCurrentItem.getItem.isInstanceOf[ItemEnderPearl]
                              || minecraft.thePlayer.inventory.getCurrentItem.getItem.isInstanceOf[ItemEgg]
                             )
                          && !minecraft.theWorld.extendedLevelsInChunkCache()){

    val projectileType = if(minecraft.thePlayer.inventory.getCurrentItem.getItem.isInstanceOf[ItemBow]) ProjectileType.ARROW else ProjectileType.OTHER

    // If the player is using a bow, compute the power increase wedge based on draw strength/progress
    val powerWedge = if(minecraft.thePlayer.inventory.getCurrentItem.getItem.isInstanceOf[ItemBow]){

        val draw    = (72000 - minecraft.thePlayer.getItemInUseCount) / 20D
        var factor  = (pow(draw, 2) + draw * 2.0) / 3F

        if(factor < 0.1F) return
        if(factor > 1.0F) factor = 1F

        factor * 2
    } else {
      1D
    }


    val arrowPath = ProjectilePathTrace( -sin(minecraft.thePlayer.rotationYaw   / 180F * Math.PI) * cos(minecraft.thePlayer.rotationPitch / 180F * Math.PI) * powerWedge * 1.5,
                                         -sin(minecraft.thePlayer.rotationPitch / 180F * Math.PI) * powerWedge * 1.5,
                                          cos(minecraft.thePlayer.rotationYaw   / 180F * Math.PI) * cos(minecraft.thePlayer.rotationPitch / 180F * Math.PI) * powerWedge * 1.5,
                                          HRenderManager.renderPosX - (cos(minecraft.thePlayer.rotationYaw / 180F * Math.PI) * 0.16F),
                                          HRenderManager.renderPosY,
                                          HRenderManager.renderPosZ - (sin(minecraft.thePlayer.rotationYaw / 180F * Math.PI) * 0.16F),
                                          minecraft.theWorld, projectileType)

    // Stride offset
    var strideOffset    = 0
    val pathVertexCount = arrowPath._2.length

    GLUtil.with3D({

      GL11.glEnable(GL11.GL_BLEND)
      GL11.glLineWidth(3F)

      GLUtil.setColour(configuration.getLong("projectiles.target_unlocked").toInt)

      if(configuration.getBoolean("projectiles.render_arc")){
        GLUtil.setColour(configuration.getLong("projectiles.target_unlocked").toInt)

        // Path render logic
        // -------------------------------------------------------------------------------------------------------------

        GL11.glBegin(GL11.GL_LINE_STRIP) // vertex-to-vertex "connect the dots" drawing mode

        while (strideOffset < pathVertexCount) {
          GL11.glVertex3d(
            arrowPath._2(strideOffset + 0) - HRenderManager.renderPosX,
            arrowPath._2(strideOffset + 1) - HRenderManager.renderPosY,
            arrowPath._2(strideOffset + 2) - HRenderManager.renderPosZ
          )
          strideOffset += 3
        }

        GL11.glEnd()

        // -------------------------------------------------------------------------------------------------------------
      }

      arrowPath._3 match {
        case Some(mop) =>

          GL11.glTranslated(
            arrowPath._2(pathVertexCount - 3) - HRenderManager.renderPosX,
            arrowPath._2(pathVertexCount - 2) - HRenderManager.renderPosY,
            arrowPath._2(pathVertexCount - 1) - HRenderManager.renderPosZ
          )

          // Rotate landing indicator to match computed hit face of collision object
          mop.sideHit match {
            case HEnumFacing.North => GL11.glRotatef(90, 1, 0, 0)
            case HEnumFacing.South => GL11.glRotatef(90, 1, 0, 0)
            case HEnumFacing.West  => GL11.glRotatef(90, 0, 0, 1)
            case HEnumFacing.East  => GL11.glRotatef(90, 0, 0, 1)
            case _ => // Nothing needed (Top/Bottom face)
          }

          GLUtil.setColour(mop.typeOfHit match {
            case HMovingObjectPosition.Type.Entity =>
              configuration.getLong("projectiles.target_locked").toInt
            case _ =>
              configuration.getLong("projectiles.target_unlocked").toInt
          })


          // Target Indicator
          // -----------------------------------------------------------------------------------------------------------

          // draw 3D targeting indicator "crosshair"
          GL11.glBegin(GL11.GL_LINES)
            GL11.glVertex3d(-1.0, 0, 0   )
            GL11.glVertex3d(-0.3, 0, 0   )

            GL11.glVertex3d(0   , 0, -1.0)
            GL11.glVertex3d(0   , 0, -0.3)

            GL11.glVertex3d(1.0 , 0, 0   )
            GL11.glVertex3d(0.3 , 0, 0   )

            GL11.glVertex3d(0   , 0, 1.0)
            GL11.glVertex3d(0   , 0, 0.3)
          GL11.glEnd()

          GL11.glRotatef(90F, 1, 0, 0)

          // draw 3D targeting indicator disks
          disk.draw(1F  , 1F  , 64 /* slices */, 1 /* loop */) // Outer ring
          disk.draw(0.3F, 0.3F, 32 /* slices */, 1 /* loop */) // Inner ring

        case None => //
      }

    })
  }

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  override def distinguishedName: String = "pw.hysteria.projectile_prediction"
}
