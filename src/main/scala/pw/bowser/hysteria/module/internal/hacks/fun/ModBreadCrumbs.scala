package pw.bowser.hysteria.module.internal.hacks.fun

import pw.bowser.hysteria.module.internal.HysteriaModule
import pw.bowser.hysteria.commands.companion.{Flag, Command}
import pw.bowser.hysteria.util.commands.ToggleFlag
import pw.bowser.hysteria.gui.Renderable
import org.lwjgl.opengl.GL11
import pw.bowser.hysteria.minecraft.Ticker
import gnu.trove.list.array.TDoubleArrayList
import pw.bowser.hysteria.display.GLUtil
import net.minecraft.client.renderer.entity.RenderManager
import pw.bowser.hysteria.commands.HysteriaCommand
import pw.bowser.hysteria.config.ConfigurationFlag
import pw.bowser.libhysteria.modules.ModuleManifest
import pw.bowser.libhysteria.toggles.TogglesMixin
import pw.bowser.minecraft.wrapper.net.minecraft.render.entity.HRenderManager

/**
 * Date: 3/1/14
 * Time: 11:46 PM
 */
@ModuleManifest(
  groupId = "pw.bowser",
  name = "BreadCrumbs",
  version = "0.2",
  commands = Array("bc"),
  description = "Those blasted lines that everyone seems to like."
)
class ModBreadCrumbs extends HysteriaModule with TogglesMixin {

  var command: HysteriaCommand = null

  // Module-Related
  //--------------------------------------------------------------------------------------------------------------------

  override def initModule(): Unit = {
    super.initModule()

    configuration.setPropertyIfNew("path.definition", 0.3D)
    configuration.setPropertyIfNew("path.length", 1500)
    configuration.setPropertyIfNew("path.colour", 0x9A5DD4)
    configuration.setPropertyIfNew("path.width", 1.3F)
    configuration.setPropertyIfNew("path.resume", false)

    command = Command(
      Array("bc", "bread"),
      ToggleFlag(Array("@if_none"), this),
      ConfigurationFlag(Array("d", "def"), configuration, "path.definition", ConfigurationFlag.Transforms.DOUBLE),
      ConfigurationFlag(Array("l", "length"), configuration, "path.length", ConfigurationFlag.Transforms.IntBetween(0, Integer.MAX_VALUE / 3)),
      ConfigurationFlag(Array("c", "colour"), configuration, "path.colour", ConfigurationFlag.Transforms.INT),
      ConfigurationFlag(Array("w", "width"), configuration, "path.width", ConfigurationFlag.Transforms.FLOAT),
      ConfigurationFlag(Array("r", "resume"), configuration, "path.resume", ConfigurationFlag.Transforms.BOOLEAN),
      Flag(
        Array("cl", "clear"), { args => lineBuffer.clear(); tellPlayer("Line buffer cleared") },
        null, "Clear the line buffer"
      ),
      Flag(
        Array("p", "pause"), { args => isPaused = !isPaused; tellPlayer("Pathing " + (if (isPaused) "paused" else "un-paused")) },
        null, "Pause pathing"
      )
    )
  }

  override def enableModule(): Unit = {
    commandDispatcher.registerCommand(command)
    toggleRegistry.enrollToggleable(this)
    super.enableModule()
  }

  override def disableModule(): Unit = {
    commandDispatcher.unRegisterCommand(command)
    toggleRegistry.disenrollToggleable(this)
    super.disableModule()
  }

  // TogglesImpl
  //--------------------------------------------------------------------------------------------------------------------

  override def disable(): Unit = {
    if (!configuration.getBoolean("path.resume")) lineBuffer.clear()
    worldRenderQueue.remove(renderJob)
    tickService.remove(tickJob)
  }

  override def enable(): Unit = {
    worldRenderQueue.register(renderJob)
    tickService.register(tickJob)
  }

  // Back-End
  //--------------------------------------------------------------------------------------------------------------------

  // Config info

  private def definition: Double = configuration.getDouble("path.definition")

  private def length: Int = configuration.getInt("path.length") * 3

  private def colour: Int = configuration.getInt("path.colour")

  private def width: Float = configuration.getFloat("path.width")

  // Actual work

  var lineBuffer = new TDoubleArrayList()

  val renderJob = Renderable({
    var renderPosCounter = 0
    GLUtil.with3D({
      GL11.glLineWidth(width)
      GLUtil.setColour(colour)
      GL11.glBegin(GL11.GL_LINE_STRIP)

      while (renderPosCounter < lineBuffer.size) {
        GL11.glVertex3d(
          lineBuffer.getQuick(renderPosCounter + 0) - HRenderManager.renderPosX,
          lineBuffer.getQuick(renderPosCounter + 1) - HRenderManager.renderPosY - minecraft.thePlayer.height,
          lineBuffer.getQuick(renderPosCounter + 2) - HRenderManager.renderPosZ)
        renderPosCounter += 3
      }
      GL11.glEnd()
    })
  })

  var lastPos = (0d, 0d, 0d)
  var isPaused = false

  val tickJob = Ticker({
    if (minecraft.theWorld != null && !isPaused) {
      if (minecraft.thePlayer.getDistance(lastPos._1, lastPos._2, lastPos._3) > definition) {
        lastPos = (minecraft.thePlayer.posX, minecraft.thePlayer.posY, minecraft.thePlayer.posZ)

        if (lineBuffer.size() >= length) lineBuffer.remove(0, 3)

        lineBuffer.add(lastPos._1)
        lineBuffer.add(lastPos._2)
        lineBuffer.add(lastPos._3)
      }
    }
  })

  def displayName: String = "BreadCrumbs"

  def distinguishedName: String = "pw.hysteria.bread_crumbs"

  override def statusText: Option[String] = Some((lineBuffer.size() / 3).toString)
}