package pw.bowser.hysteria.module.internal.util.special.citadel

import java.io.{OutputStream, InputStream}

import scala.collection.{mutable => m}
import scala.util.parsing.json.{JSON, JSONArray, JSONObject}
import pw.bowser.hysteria.util.scala.json.Formatters
import scala.io.Source
import pw.bowser.libhysteria.storage.marshal.Marshalling

/**
 * Date: 3/24/14
 * Time: 11:13 PM
 */
class CitadelProtectionCache extends Marshalling {

  private val pool = m.Map[(Long, Long, Long), CitadelBlock]()

  /**
   * Marshal self to output stream
   *
   * @param stream stream to write to
   */
  def writeTo(stream: OutputStream): Unit = {
    val json = JSONObject(pool.map({case(tuple, block) =>
      tuple.toString() -> JSONObject(Map("position" -> JSONArray(List(tuple._1, tuple._2, tuple._3)), "block" -> block.toJson))
    }).toMap).toString(Formatters.prettyFormatter)

    stream.write(json.getBytes)
  }

  /**
   * Load self from input stream
   *
   * @param stream stream to read from
   */
  def readFrom(stream: InputStream): Unit = {
    val lines       = Source.createBufferedSource(stream).getLines().mkString("\n")
    val json        = JSON.parseRaw(lines)
    val storedData  = json.asInstanceOf[JSONObject].obj

    val loadedData  = storedData.map({case(key, jsonObject: JSONObject) =>
      val dataKeyList   = jsonObject.obj("position").asInstanceOf[JSONArray].list.map(_.asInstanceOf[Double].toLong)
      val dataKey       = dataKeyList match {case List(x, y, z) => (x, y, z)}

      val blockData     = jsonObject.obj("block").asInstanceOf[JSONObject]

      val blockInstance = pool.getOrElse(dataKey,
        new CitadelBlock(dataKey._1, dataKey._2, dataKey._3, ProtectionType.UNKNOWN,
          ProtectionType.ProtectionLevel.UNKNOWN, this))

      blockInstance.fromJson(blockData)

      dataKey -> blockInstance
    })

    pool.clear()
    loadedData.foreach({case(key, value) => pool.put(key, value)})
  }

  /**
   * Remove a block from the cache
   * @param x block X
   * @param y block Y
   * @param z block Z
   * @return block, if any
   */
  def removeFromCache(x: Int, y: Int, z: Int): Option[CitadelBlock] = pool.remove((x, y, z))

  /**
   * Get the citadel block at the specified position
   * @param x block x
   * @param y block y
   * @param z block z
   * @return block
   */
  def getBlockAt(x: Int, y: Int, z: Int): Option[CitadelBlock] = pool.get((x, y, z))

  /**
   * Clear the cache
   */
  def clearCache(): Unit = pool.clear()

  /**
   * Get the handle of the marshallable (for ref like filename etc)
   *
   * @return handle
   */
  def getHandle: String = "citadel_cache.json"
}
