package pw.bowser.minecraft.wrapper.net.minecraft.item

import net.minecraft.item.Item

class HItem(val mcItem: Item) extends AnyVal {

    def id: Int = HItem.idOf(mcItem)

}

object HItem {
    def withId(id: Int): Option[Item] = Option(Item.getItemById(id))

    def withNameOrId(name: String): Option[Item] = Option(Item.getByNameOrId(name))

    def idOf(item: Item): Int = Item.getIdFromItem(item)
}
