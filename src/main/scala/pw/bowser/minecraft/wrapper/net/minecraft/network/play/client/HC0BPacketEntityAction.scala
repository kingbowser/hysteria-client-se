package pw.bowser.minecraft.wrapper.net.minecraft.network.play.client

import net.minecraft.network.play.client.C0BPacketEntityAction

/**
 * Date: 2/14/16
 * Time: 5:58 PM
 */
class HC0BPacketEntityAction(val mcPacket: C0BPacketEntityAction) extends AnyVal {

    import HC0BPacketEntityAction.Action

    def entityID: Int = classOf[C0BPacketEntityAction].getField("field_149517_a").getInt(mcPacket)

    def action: Action = mcPacket.func_180764_b()

    def auxData: Int = mcPacket.func_149512_e()

}

object HC0BPacketEntityAction {
    import C0BPacketEntityAction.{Action=>PAction}
    type Action = PAction
    val StartSneaking   = PAction.START_SNEAKING
    val StopSneaking    = PAction.STOP_SNEAKING
    val StopSleeping    = PAction.STOP_SLEEPING
    val StopSprinting   = PAction.STOP_SPRINTING
    val RidingJump      = PAction.RIDING_JUMP
    val OpenInventory   = PAction.OPEN_INVENTORY
}
