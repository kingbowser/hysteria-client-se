package pw.bowser.minecraft.wrapper.net.minecraft.entity

import net.minecraft.entity.Entity
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.AxisAlignedBB

/**
 * Date: 2/14/16
 * Time: 12:58 PM
 */
final class HEntity(val mcEntity: Entity) extends AnyVal {

    def boundingBox: AxisAlignedBB = mcEntity.getEntityBoundingBox

}
