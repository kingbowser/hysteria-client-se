package pw.bowser.minecraft.wrapper.net.minecraft.util

import pw.bowser.minecraft.wrapper.MCTypes

/**
 * Date: 2/14/16
 * Time: 3:13 PM
 */
class HVector3(val mcv3: MCTypes.Vector3) extends AnyVal {

    def x: Double = mcv3.xCoord

    def y: Double = mcv3.yCoord

    def z: Double = mcv3.zCoord

    def +(other: MCTypes.Vector3): MCTypes.Vector3 = mcv3.add(other)

    def -(other: MCTypes.Vector3): MCTypes.Vector3 = mcv3.subtract(other)

}
