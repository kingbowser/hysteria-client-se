package pw.bowser.minecraft.wrapper.net.minecraft.entity.player

import java.util.UUID

import com.mojang.authlib.GameProfile
import net.minecraft.entity.player.EntityPlayer
import net.minecraft.util.AxisAlignedBB
import pw.bowser.hysteria.engine.HysteriaBroker
import pw.bowser.minecraft.players.PlayerData

/**
 * Wrapper for EntityPlayer to provide functions that Hysteria _should_ use.
 */
final class HEntityPlayer(val mcEntityPlayer: EntityPlayer) extends AnyVal {

    def commandSenderName: String = mcEntityPlayer.getGameProfile.getName

    def gameProfile: GameProfile = mcEntityPlayer.getGameProfile

    def uuid: UUID = gameProfile.getId

    def playerData: PlayerData = HysteriaBroker.hysteria.playerData.lookupPlayer(gameProfile.getName)


}
