package pw.bowser.minecraft.wrapper.net.minecraft.util

import net.minecraft.util.MovingObjectPosition.MovingObjectType
import net.minecraft.util.{EnumFacing, MovingObjectPosition}

/**
 * Date: 7/13/16
 * Time: 10:20 AM
 */
class HMovingObjectPosition(val wrapped: MovingObjectPosition) extends AnyVal {

  def sideHit: EnumFacing = wrapped.field_178784_b

  def typeOfHit: MovingObjectType = wrapped.typeOfHit

}

object HMovingObjectPosition {
  object Type {
    val Miss    = MovingObjectType.MISS
    val Block   = MovingObjectType.BLOCK
    val Entity  = MovingObjectType.ENTITY
  }
}
