package pw.bowser.minecraft.wrapper.net.minecraft.network.play.server

import net.minecraft.network.play.server.{S12PacketEntityVelocity, S08PacketPlayerPosLook}

/**
 * Date: 2/14/16
 * Time: 2:49 PM
 */
trait HServerPacketConversions {

    implicit def HS08forMCS08(mcPacket: S08PacketPlayerPosLook): HS08PacketPlayerPosLook = new HS08PacketPlayerPosLook(mcPacket)

    implicit def HS12forMCS12(mcPacket: S12PacketEntityVelocity): HS12PacketEntityVelocity = new HS12PacketEntityVelocity(mcPacket)

}
