package pw.bowser.minecraft.wrapper.net.minecraft.client.gui

import net.minecraft.client.gui.{ChatLine, GuiNewChat}
import net.minecraft.util.{ChatComponentText, IChatComponent}
import pw.bowser.hysteria.chat.Formatting
import pw.bowser.minecraft.wrapper.Wrapper

final class HGuiNewChat(val mcGuiNewChat: GuiNewChat) extends AnyVal {

  import java.util.{List => JList}

  def addChatMessage(component: IChatComponent, deletion: Int = 0): Unit = {
      mcGuiNewChat.printChatMessageWithOptionalDeletion(component, deletion)
  }

  def addChatMessage(str: String): Unit = {
      addChatMessage(new ChatComponentText(s"[${Formatting.DARK_GREEN}HYS${Formatting.RESET}] $str"))
  }

  def width: Int = mcGuiNewChat.getChatWidth

  def height: Int = mcGuiNewChat.getChatHeight

  def scale: Float = mcGuiNewChat.getChatScale

  def splitChatLines: JList[ChatLine] = {
    Wrapper.getField[GuiNewChat]("chatLines").get(mcGuiNewChat).asInstanceOf[JList[ChatLine]]
  }

  def unsplitChatLines: JList[ChatLine] = {
    Wrapper.getField[GuiNewChat]("field_146253_i").get(mcGuiNewChat).asInstanceOf[JList[ChatLine]]
  }

}
