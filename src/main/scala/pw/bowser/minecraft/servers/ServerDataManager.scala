package pw.bowser.minecraft.servers

import net.minecraft.client.multiplayer.{ServerData => MCServerData}

/**
 * Specification for server data manager implementations
 * Date: 3/24/14
 * Time: 9:52 PM
 */
trait ServerDataManager {

  /**
   * Get the ServerData for the specified server
   * @param mcData  minecraft server data
   * @return        server data
   */
  def getDataFor(mcData: MCServerData): ServerData

  /**
   * Add an initialization hook for ServerData creation
   * @param hook initialization hook
   */
  def registerInitializeHook(hook: ServerData => _): Unit

  /**
   * Remove an initialization hook
   * @param hook initialization hook
   */
  def removeInitializeHook(hook: ServerData => _): Unit

}
