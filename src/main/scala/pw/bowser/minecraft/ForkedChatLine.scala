package pw.bowser.minecraft

import net.minecraft.client.gui.ChatLine
import net.minecraft.util.IChatComponent

/**
 * ChatLine that forks according to an expression
 * Date: 2/18/14
 * Time: 10:34 AM
 */
final class ForkedChatLine(createdAt: Int,
                           lineId: Int,
                           compoFork: IChatComponent,
                           component: IChatComponent,
                           forkClause: => Boolean)
                           extends ChatLine(createdAt, component, lineId) {

  override def getChatComponent(/* empty paren required despite scalac note */): IChatComponent = forkClause match {
    case true   => compoFork
    case false  => super.getChatComponent()
  }

}
