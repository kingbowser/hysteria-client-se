package pw.bowser.hysteria.inject;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Denotes a field that should be injected with instances of configuration-intended data
 *
 * Date: 1/29/14
 * Time: 8:42 PM
 */
@Target({ElementType.FIELD, ElementType.PARAMETER, ElementType.LOCAL_VARIABLE})
@Retention(RetentionPolicy.RUNTIME) @BindingAnnotation
public @interface Config {
}
