package pw.bowser.hysteria.minecraft;

import java.util.ArrayList;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Handles rendering batches of minecraft renderables
 * Not entities. Use minecraft for that.
 *
 * Date: 1/23/14
 * Time: 11:40 AM
 */
public class RenderQueue implements RenderJob {

    private ArrayList<RenderJob> jobs  = new ArrayList<>();
    private Queue<RenderJob> newJobs   = new ArrayBlockingQueue<>(10);
    private Queue<RenderJob> delJobs   = new ArrayBlockingQueue<>(10);

    public void register(RenderJob job){
        newJobs.add(job);
    }

    public void remove(RenderJob job){
        delJobs.add(job);
    }

    public void render() {
        while(!delJobs.isEmpty()) jobs.remove(delJobs.remove());
        while(!newJobs.isEmpty()) jobs.add(newJobs.remove());

        for(RenderJob job : jobs) try {
//            GL11.glPushMatrix();
            job.render();
//            GL11.glPopMatrix();
        } catch(Throwable t) {
            System.err.println(t.getCause());
            t.printStackTrace();
            //TODO exception handling needed
        }
    }

}
