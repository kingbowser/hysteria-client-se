package pw.bowser.hysteria.minecraft;

/**
 * Receives a method call on minecraft tick
 *
 * Date: 1/23/14
 * Time: 10:34 AM
 */
public interface TickReceiver {

    /**
     * Called on a tick
     */
    public void onTick();

}
