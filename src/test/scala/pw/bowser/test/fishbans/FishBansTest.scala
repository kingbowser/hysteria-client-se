package pw.bowser.test.fishbans

import org.junit.Test
import pw.bowser.fishbans.FishBans

/**
 * Date: 2/23/14
 * Time: 12:19 PM
 */
class FishBansTest {

  @Test def testFishBans() = {

    val player  = "roby718"
    val profile = FishBans.profileOf(player)

    println(s"$player's url is ${FishBans.PLAYER_BANS.format(player)}")

    println(s"$player's uuid is ${profile.get.userId}")
    println(s"$player's bans are ${profile.get.serviceBans}")

    assert(FishBans.isBanned(player))

  }

}
