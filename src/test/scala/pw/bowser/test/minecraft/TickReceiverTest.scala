package pw.bowser.test.minecraft

import org.junit.Test
import pw.bowser.hysteria.minecraft.{TickReceiver, TickGroup}
import scala.actors.Actor

/**
 * Date: 1/23/14
 * Time: 11:01 AM
 */
class TickReceiverTest {

  @Test def testGroupWorks() {
    var v1: Boolean = false
    var v2: Boolean = false
    var v3: Boolean = false

    val group: TickGroup = new TickGroup

    val v1R: TickReceiver = new TickReceiver {
      def onTick(): Unit = v1 = true
    }

    val v2R: TickReceiver = new TickReceiver {
      def onTick(): Unit = v2 = true
    }

    val v3R: TickReceiver = new TickReceiver {
      def onTick(): Unit = v3 = true
    }

    group register v1R
    group register v2R
    group register v3R

    group.onTick()

    assert(v1 && v2 & v3)
  }

  @Test def testJoinAtRuntimeWorks() {

    var v1: Boolean = false
    var v2: Boolean = false
    var v3: Boolean = false

    val group: TickGroup = new TickGroup

    val v1R: TickReceiver = new TickReceiver {
      def onTick(): Unit = v1 = true
    }

    val v2R: TickReceiver = new TickReceiver {
      def onTick(): Unit = v2 = true
    }

    val v3R: TickReceiver = new TickReceiver {
      def onTick(): Unit = v3 = true
    }

    val tickThread: TickGroupActor = new TickGroupActor(group)
    tickThread.start()

    group register v1R
    group register v2R
    group register v3R

    Thread.sleep(10000)

    assert(v1 && v2 && v3)
  }

  private class TickGroupActor(private val group: TickGroup) extends Actor {

    private var running: Boolean = true

    def act() {
      while(running){
        group.onTick()
        Thread.sleep(1000)
      }
    }

    def stop() = running = false
  }

}