Hysteria (Minecraft Client)
===========================

# [READ THE LICENSE FILE](https://bitbucket.org/kingbowser/hysteria-client-se/src/master/LICENSE)

Hysteria is a minecraft client that attempts to ease both trolling and server administration.

Features
--------

* Some NC bypasses
* Cool data systems
* Pluggability
* Real-world project structure -- build scripts, submodules, and release automation.
* Uses Optifine-B* for MC 1.7.2

Building
--------
*I am assuming that you have `git` in your PATH*

1. Clone the repository
2. After cloning the repository, `git submodule init` and then `git submodule update`
3. Before continuing, insure that you have generated a default GPG key
4. Ensure that a folder named `release-mc` exists
5. Execute `gradle -x test mcRelease`
6. You will find your release package in `./release-mc`

### Please note

* If and when the custom launcher comes out, you will be unable to use your packages with the launcher, unless you built the launcher from source using your GPG pubkey

### Help with building:

I will only be glad to help you if

* A) You run linux and use Oracle's JDK 1.7u45 or better
* B) You run windows and have a properly configured Cygwin environment with gradle/java

Contributing
------------

The `master` will be protected soon. Please create a branch for your development!
